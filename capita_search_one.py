# import requests
import feedparser
from flask import Flask, render_template, jsonify,request
from urllib import quote_plus

app = Flask(__name__)

capita_endpoint = "http://capitadiscovery.co.uk/%s/items.rss?query=%s&offset=0"

capita_libraries = ["brighton-ac","leeds","lancashire","worcs","cityoflondon","gloslibraries","uclan","conwy","eastlothian","leicestershire","herefordshire","bradford","rotherham","northampton-ac","gateshead","cornwall","highland","aberdeenshire","mmu","greenwich-ac","gsa","aberdeencity","ul","torfaen","coventry","poole","medway-ac","birmingham"]

req_headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36'} #let's pretend to be chrome

query_cache = {}


@app.route("/")
def index():
	return render_template("main.html")


@app.route("/metasearch", methods=['POST'])
def metasearch():
	if request.method == 'POST':
		raw_query = request.form['query']
		query = quote_plus(raw_query)
		results = {}

		for library in capita_libraries:
			results[library] = []

			request_url = capita_endpoint % (library,query)
			result = feedparser.parse(request_url)
			
			items = len(result['items'])

			# print library, items
			if items == 0:
				query_cache['library'] = "No Match"
				results[library].append("No Matches Found")

			else:
				query_cache['library'] = result
				for item in result['items']:
					results[library].append(item.links[0].href)
		print results

		return jsonify(result=results)



  

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=5001, debug=True)
