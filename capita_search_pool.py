from multiprocessing import Pool, Manager
import feedparser
from flask import Flask, render_template, jsonify,request
from werkzeug.contrib.cache import MemcachedCache
try:
	from boto.utils import get_instance_metadata
	m = get_instance_metadata()
	if len(m.keys()) > 0:
		cache = MemcachedCache(['metacache.hfpv77.cfg.use1.cache.amazonaws.com:11211'])
	else:
		raise Exception('E_NOTAWS')
except:
	cache = MemcachedCache(['127.0.0.1:11211'])
from urllib import quote_plus
MAX_THREADS = 12
application = app = Flask(__name__)

capita_endpoint = "http://capitadiscovery.co.uk/%s/items.rss?query=%s&offset=0"

#capita_libraries = ["brighton-ac","leeds","lancashire","worcs","cityoflondon","gloslibraries","uclan","conwy","eastlothian","leicestershire","herefordshire","bradford","rotherham","northampton-ac","gateshead","cornwall","highland","aberdeenshire","mmu","greenwich-ac","gsa","aberdeencity","ul","torfaen","coventry","poole","medway-ac","birmingham"]
capita_libraries = ['southwales-ac', 'fife', 'southtyneside', 'cardiff-met', 'hartlepool', 'birmingham', 'gllm', 'northampton-ac', 'brighton-ac', 'aberdeenshire', 'medway-ac', 'gwynedd', 'hope', 'north-ayrshire', 'ulster-ac', 'eastrenfrewshire', 'uwl', 'shetland', 'brookes', 'bournemouth', 'rutland', 'harper-adams', 'mdi', 'tvu', 'wigan', 'bradfordcollege', 'roehampton', 'newman-ac', 'islington', 'southwark', 'smu', 'poole', 'surrey-ac', 'rhondda', 'mmu', 'merthyr', 'aberdeencity', 'leeds', 'sussex-ac', 'dcu', 'rotherham', 'cityoflondon', 'winchester-ac', 'greenwich-ac', 'cumbria-ac', 'uclan-cyprus', 'ncad', 'leicestershire', 'highland', 'jersey', 'moray', 'wolverhampton', 'stockton', 'teesside-ac', 'menai-ac', 'ucd', 'swindon', 'orkney', 'rcs', 'eastlothian', 'conwy', 'northyorkshire', 'rml', 'broadminster', 'northumbria-ac', 'liverpool', 'sunderland-ac', 'bham-ac', 'cornwall', 'haringey', 'barnsleymbc', 'torfaen', 'coventry', 'herefordshire', 'chi-ac', 'ul', 'dmu', 'gateshead', 'landrillo-ac', 'solihull', 'derby-ac', 'spd', 'worc-ac', 'myerscough-ac', 'perthandkinross', 'mic', 'teeside-ac', 'port', 'glam-ac', 'ynysmon', 'worcs', 'edinburgh', 'lambeth', 'uclan', 'bolton-ac', 'wirral', 'gloslibraries', 'bradford', 'lancashire', 'gsa', 'gloucestershire', 'guernsey', 'wlv-ac', 'bcu', 'bolton']

req_headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36'} 
#let's pretend to be chrome


@app.route("/")
def index():
	return render_template("main.html")

def search_library(library, query, results_proxy):
	result_store = []
	cache_key = library + "_" + query
	print cache_key
	cv = cache.get(cache_key)
	if cv is None:

		request_url = capita_endpoint % (library,query)
		result = feedparser.parse(request_url)
		print request_url
		items = len(result['items'])
		if items != 0:
			# result_store.append("No Matches Found")
		# else:
			for item in result['items']:
				display_object = {}
				display_object['href'] = item.links[0].href
				display_object['title'] = item.title
				display_object['publisher'] = item.publisher
				display_object['author'] = item.author
				display_object['isbn'] = item.bibo_isbn
				display_object['type'] = item.dc_format

				result_store.append(display_object)

		cache.set(cache_key,result_store,timeout=172800) #48 hour cache expiry

		results_proxy[library] = result_store
	else:
		print library, cv, type(cv)
		results_proxy[library] = cv



@app.route("/metasearch", methods=['POST'])
def metasearch():
	if request.method == 'POST':
		raw_query = request.form['query']

		query = quote_plus(raw_query.rstrip().lstrip())
		manager = Manager()
		results = manager.dict()
		pool = Pool(processes=MAX_THREADS)

		for library in capita_libraries:
			print library
			pool.apply_async(search_library,[library,query,results])
		pool.close()
		pool.join()

		sane_results = {}
		for key in results.keys():
			if results[key] == []:
				continue
			sane_results[key] = results[key]
		return jsonify(result=sane_results)



  

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=5001, debug=True)
